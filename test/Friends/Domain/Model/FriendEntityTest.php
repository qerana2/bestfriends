<?php

namespace Friends\Domain\Model;

use App\Common\Domain\Exception\InvalidEmailException;
use App\Common\Domain\Exception\InvalidPhoneNumberException;
use App\Friends\Domain\Model\Friend;
use PHPUnit\Framework\TestCase;

class FriendEntityTest extends TestCase
{


    /**
     * @group invalid
     */
    public function testIfThrowExceptionToInvalidEmail()
    {
        $this->expectException(InvalidEmailException::class);
        $friend = new Friend('James bond', 'jaime007', 'invalid', '685714934');

    }

    /**
     * @group invalid
     */
    public function testIfTrowExceptionToInvalidPhoneNumberWithoutLength()
    {
        $this->expectException(InvalidPhoneNumberException::class);
        $friend = new Friend('James bond', 'jaime007', 'valid@gmail.com', '68571493');
    }

    /**
     * @group invalid
     */
    public function testIfTrowExceptionOnEmptyPhone()
    {
        $this->expectException(InvalidPhoneNumberException::class);
        $friend = new Friend('James bond', 'jaime007', 'valid@gmail.com', '');
    }

    /**
     * @group invalid
     */
    public function testIfTrowExceptionToInvalidPhoneNumber()
    {
        $this->expectException(InvalidPhoneNumberException::class);
        $friend = new Friend('James bond', 'jaime007', 'valid@gmail.com', '475398716');
    }

    /**
     * @group happy
     */
    public function testIfEmailFriendWasUpdated()
    {
        $friend = new Friend('James bond', 'jaime007', 'james@friend.com', '685714934');
        $friend->update([
            'email' => 'paper@mmm.com'
        ]);
        $this->assertEquals('paper@mmm.com',$friend->getEmail());

    }
    /**
     * @group happy
     */
    public function testIfNameFriendWasUpdated()
    {
        $friend = new Friend('James bond', 'jaime007', 'james@friend.com', '685714934');
        $friend->update([
            'name' => 'mark ruddes'
        ]);
        $this->assertEquals('mark ruddes',$friend->getName());

    }
    /**
     * @group happy
     */
    public function testIfPhoneFriendWasUpdated()
    {
        $friend = new Friend('James bond', 'jaime007', 'james@friend.com', '685714934');
        $friend->update([
            'phone_number' => '715964173'
        ]);

        $this->assertEquals('715964173',$friend->getPhoneNumber());

    }
    /**
     * @group happy
     */
    public function testIfNicknameFriendWasUpdated()
    {
        $friend = new Friend('James bond', 'jaime007', 'james@friend.com', '685714934');
        $friend->update([
            'nickname' => 'newname01'
        ]);

        $this->assertEquals('newname01',$friend->getNickname());

    }

}