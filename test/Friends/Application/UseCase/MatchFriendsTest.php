<?php

namespace Friends\Application\UseCase;

use App\Friends\Application\UseCase\MatchFriends;
use App\Friends\Domain\Exception\EmptyFriendsException;
use App\Friends\Domain\Model\Friend;
use App\Friends\Infrastructure\Storage\EmptyFriendRepository;
use App\Friends\Infrastructure\Storage\InMemoryFriendRepository;
use PHPUnit\Framework\TestCase;


class MatchFriendsTest extends TestCase
{


    /**
     * @group happy
     */
    public function testIfThrowExceptionEmptyFriends()
    {

        $this->expectException(EmptyFriendsException::class);
        $match = new MatchFriends(new EmptyFriendRepository());
        $match();

    }

    /**
     * @group happy
     */
    public function testMatching()
    {
        $memoryRepository = new InMemoryFriendRepository();
        // let's create 3 friends
        $friend1 = new Friend('amigo1 apellido1', 'amig1', 'email1@email.com', '987132456');
        $friend2 = new Friend('amigo2 apellido1', 'amig2', 'email2@email.com', '687132456');
        $friend3 = new Friend('lars kubrik', 'lars2', 'lars2@metallica.com', '787132456');
        $memoryRepository->save($friend1);
        $memoryRepository->save($friend2);
        $memoryRepository->save($friend3);

        // match
        $match = new MatchFriends($memoryRepository);
        $result = $match();

        $this->assertEquals(3,sizeof($result));

    }

    public function testIfFriendDontMatchOnSelf(){

        $memoryRepository = new InMemoryFriendRepository();
        // let's create 3 friends
        $friend1 = new Friend('amigo1 apellido1', 'amig1', 'email1@email.com', '987132456');
        $friend2 = new Friend('amigo2 apellido1', 'amig2', 'email2@email.com', '687132456');
        $friend3 = new Friend('lars kubrik', 'lars2', 'lars2@metallica.com', '787132456');
        $memoryRepository->save($friend1);
        $memoryRepository->save($friend2);
        $memoryRepository->save($friend3);

        // match
        $match = new MatchFriends($memoryRepository);
        $result = $match();

        $id_friend0 = $result[0]['friend']->getIdFriend();
        $id_match_friend0 = $result[0]['match']->getIdFriend();

        $id_friend1 = $result[1]['friend']->getIdFriend();
        $id_match_friend1 = $result[1]['match']->getIdFriend();

        $id_friend2 = $result[2]['friend']->getIdFriend();
        $id_match_friend2 = $result[2]['match']->getIdFriend();

        $this->assertNotEquals($id_friend0,$id_match_friend0);
        $this->assertNotEquals($id_friend1,$id_match_friend1);
        $this->assertNotEquals($id_friend2,$id_match_friend2);


    }


}