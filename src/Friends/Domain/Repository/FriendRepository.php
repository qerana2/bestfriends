<?php

namespace App\Friends\Domain\Repository;

use App\Friends\Domain\Model\Friend;

interface FriendRepository
{


    public function allOrFail(): array;

    public function all(): array;

    public function save(Friend $friend): Friend;

    public function findById(string $id): ?Friend;

    public function findByEmail(string $email): ?Friend;

    public function findByPhone(string $number) : ?Friend;

    public function findByNickname(string $nickname) : ?Friend;

    public function findByEmailOrFail(string $email): Friend;

    public function findByPhoneOrFail(string $number): Friend;

    public function findByNickNameOrFail(string $number): Friend;
}