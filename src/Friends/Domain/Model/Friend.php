<?php

namespace App\Friends\Domain\Model;

use App\Common\Domain\Exception\InvalidEmailException;
use App\Common\Domain\Exception\InvalidPhoneNumberException;
use App\Common\Domain\Model\Traits\TimestampTrait;
use Symfony\Component\Uid\Uuid;

class Friend
{

    // to log, timestamp created/updated
    use TimestampTrait;


    private string $id_friend;
    private string $name;
    private string $nickname;
    private string $email;
    private string $phone_number;

    /**
     * @param string $name
     * @param string $nickname
     * @param string $email
     * @param string $phone_number
     */
    public function __construct(string $name, string $nickname, string $email, string $phone_number)
    {
        $this->id_friend = Uuid::v4()->toRfc4122();
        $this->name = $name;
        $this->nickname = $nickname;
        $this->email($email);
        $this->phone($phone_number);
        $this->setCreatedOn();
    }

    /**
     * @param string $email
     * @todo, use ValueObject!!
     */
    private function email(string $email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
            throw new InvalidEmailException($email);
        }

        $this->email = $email;
    }

    /**
     * @param string $number
     * @todo, use ValueObject!!, and check spanish phone numbers (start with (6,9,7 etc))
     */
    private function phone(string $number)
    {
        // check length
        if (strlen($number) != 9) {
            throw new InvalidPhoneNumberException($number);
        }

        $valid_numbers = ['6', '7', '9'];
        $first_number = substr($number, 0, 1);

        if (!in_array($first_number, $valid_numbers)) {
            throw new InvalidPhoneNumberException($number);
        }


        $this->phone_number = $number;
    }

    /**
     * Update friend
     * @param array $data
     */
    public function update(array $data)
    {

        if (!empty($data['name'])) {
            $this->name = $data['name'];
        }
        if (!empty($data['nickname'])) {
            $this->nickname = $data['nickname'];
        }
        if (!empty($data['email'])) {
            $this->email($data['email']);
        }
        if (!empty($data['phone_number'])) {
            $this->phone($data['phone_number']);
        }

        $this->setUpdatedOn();

    }

    /**
     * @return string
     */
    public function getIdFriend(): string
    {
        return $this->id_friend;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }


    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phone_number;
    }


    public function __toString(): string
    {
        return $this->nickname;
    }

}