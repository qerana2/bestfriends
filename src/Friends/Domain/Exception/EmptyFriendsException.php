<?php

namespace App\Friends\Domain\Exception;

use InvalidArgumentException;
use Throwable;

class EmptyFriendsException extends InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct('Sorry, do you not have any friends to match!!!');
    }
}