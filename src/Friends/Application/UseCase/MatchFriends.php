<?php

namespace App\Friends\Application\UseCase;

use App\Friends\Domain\Repository\FriendRepository;

/**
 * Match friends with another friend
 */
class MatchFriends
{

    private array $matched = [];
    private array $result = [];
    private FriendRepository $friendRepository;

    /**
     * @param FriendRepository $friendRepository
     */
    public function __construct(FriendRepository $friendRepository)
    {
        $this->friendRepository = $friendRepository;
    }

    /**
     * @return array
     */
    public function __invoke(): array
    {
        // get all friends
        $friends = $this->friendRepository->allOrFail();
        $iteration = 0;

        do {

            // choose a random friend
            $random_friend = $friends[array_rand($friends)];

            // check if the random friend was not matched with another friend
            if (!in_array($random_friend, $this->matched, true)) {

                // match , always if not the same
                if ($friends[$iteration] !== $random_friend) {
                    // match friend
                    array_push($this->matched, $random_friend);
                    // add to result array
                    $this->result[] = [
                        'friend' => $friends[$iteration],
                        'match' => $random_friend
                    ];

                    $iteration++;
                }

            }

        } while ($iteration < count($friends));


        return $this->result;


    }

}