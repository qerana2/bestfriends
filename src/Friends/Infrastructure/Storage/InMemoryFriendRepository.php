<?php

namespace App\Friends\Infrastructure\Storage;

use App\Friends\Domain\Exception\EmptyFriendsException;
use App\Friends\Domain\Model\Friend;
use App\Friends\Domain\Repository\FriendRepository;

class InMemoryFriendRepository implements FriendRepository
{

    private $friends = [];


    public function all(): array
    {
        return $this->friends;
    }

    /**
     * @return array
     */
    public function allOrFail(): array
    {
       $friends = $this->all();

       if(sizeof($friends) === 0){
            throw new EmptyFriendsException();
       }
       return $friends;
    }

    public function save(Friend $friend): Friend
    {
        $this->friends[] = $friend;
        return $friend;
    }

    public function findById(string $id): ?Friend
    {
        return $this->friends[$id];
    }

    public function findByEmail(string $email): ?Friend
    {
        // TODO: Implement findByEmail() method.
    }

    public function findByPhone(string $number): ?Friend
    {
        // TODO: Implement findByPhone() method.
    }

    public function findByNickname(string $nickname): ?Friend
    {
        // TODO: Implement findByNickname() method.
    }

    public function findByEmailOrFail(string $email): Friend
    {

    }

    public function findByPhoneOrFail(string $number): Friend
    {
        // TODO: Implement findByPhoneOrFail() method.
    }

    public function findByNickNameOrFail(string $number): Friend
    {
        // TODO: Implement findByNickNameOrFail() method.
    }
}