<?php

namespace App\Friends\Infrastructure\Storage;

use App\Friends\Domain\Exception\EmptyFriendsException;
use App\Friends\Domain\Model\Friend;
use App\Friends\Domain\Repository\FriendRepository;

class EmptyFriendRepository implements FriendRepository
{

    public function allOrFail(): array
    {
        throw new EmptyFriendsException();
    }

    public function all():array
    {
        return [];
    }

    public function save(Friend $friend): Friend
    {
        // TODO: Implement save() method.
    }

    public function findById(string $id): ?Friend
    {
        // TODO: Implement findById() method.
    }

    public function findByEmail(string $email): ?Friend
    {
        // TODO: Implement findByEmail() method.
    }

    public function findByPhone(string $number): ?Friend
    {
        // TODO: Implement findByPhone() method.
    }

    public function findByNickname(string $nickname): ?Friend
    {
        // TODO: Implement findByNickname() method.
    }

    public function findByEmailOrFail(string $email): Friend
    {
        // TODO: Implement findByEmailOrFail() method.
    }

    public function findByPhoneOrFail(string $number): Friend
    {
        // TODO: Implement findByPhoneOrFail() method.
    }

    public function findByNickNameOrFail(string $number): Friend
    {
        // TODO: Implement findByNickNameOrFail() method.
    }
}