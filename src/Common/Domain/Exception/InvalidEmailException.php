<?php

namespace App\Common\Domain\Exception;

use InvalidArgumentException;
use Throwable;

class InvalidEmailException extends InvalidArgumentException
{

    public function __construct($message)
    {
        parent::__construct(sprintf('Invalid email "%s"!!',$message));
    }

}