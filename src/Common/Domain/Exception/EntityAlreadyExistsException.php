<?php

namespace App\Common\Domain\Exception;

use InvalidArgumentException;
use Throwable;

class EntityAlreadyExistsException extends InvalidArgumentException
{

    public function __construct($entity,$message = '')
    {
        parent::__construct(sprintf('Entity "%s", with "%s", already exists!!',$entity,$message));
    }

}