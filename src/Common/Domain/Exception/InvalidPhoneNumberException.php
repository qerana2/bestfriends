<?php

namespace App\Common\Domain\Exception;

use InvalidArgumentException;

class InvalidPhoneNumberException extends InvalidArgumentException
{

    public function __construct($message)
    {
        parent::__construct(sprintf('Invalid phone-number "%s"!!', $message));
    }

}