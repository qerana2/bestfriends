<?php

namespace App\Common\Domain\Exception;

use InvalidArgumentException;
use Throwable;

class EntityDoesNotExistsException extends InvalidArgumentException
{

   public function __construct($entity,$message = '')
    {
        parent::__construct(sprintf('Entity "%s", with "%s" does not exists!!',$entity,$message));
    }

}