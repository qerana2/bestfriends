<?php

namespace App\Common\Domain\Model\Traits;

use DateTime;

trait TimestampTrait
{

    private DateTime $createdOn;
    private DateTime $updatedOn;

    /**
     */
    private function setCreatedOn(): void
    {
        $this->createdOn = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getCreatedOn(): DateTime
    {
        return $this->createdOn;
    }

    /**
     */
    private function setUpdatedOn(): void
    {
        $this->updatedOn = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getUpdatedOn():DateTime
    {
        return $this->updatedOn;
    }




}