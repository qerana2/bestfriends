<?php

namespace App\Welcome;


use Qerana\Core\QeranaController;
use Symfony\Component\HttpFoundation\Response;

class WelcomeController extends QeranaController
{

    public function index(): Response
    {
        return $this->response('Hi,this is a welcome page');
    }


}