<?php

namespace App\Welcome\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;

class LoginFailEvent extends Event
{


    public const NAME = 'login.fail';

    private $occurred_on;
    private $remote_agent;
    private $remote_ip;
    private $error;


    public function __construct(Request $request,string $error)
    {
        $this->occurred_on = date('Y-m-d H:i:s');
        $this->remote_ip = $request->getClientIp();
        $this->remote_agent = $request->headers->get('user-agent');
        $this->error = $error;
    }

    /**
     * @return array
     */
    public function getFail(): array
    {
        return [
            'occurred_on' => $this->occurred_on,
            'remote_agent' => $this->remote_agent,
            'remote_ip' => $this->remote_ip,
            'error' => $this->error
        ];
    }

}
