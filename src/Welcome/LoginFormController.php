<?php

namespace App\Welcome;


use App\Welcome\Handler\LoginHandler;
use Exception;
use Qerana\Core\QeranaController;
use Qerana\Security\Model\UserAdaRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class LoginFormController extends QeranaController
{

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function login(Request $request)
    {

        $username = $request->get('f_username');
        $password = $request->get('f_password');

        $loginHandler = new LoginHandler(
            new UserAdaRepository(),
            $request,
            $this->dispatcher
        );
        $loginHandler->handle($username, $password);

        $redirect_path = $request->get('redirect');

        return new RedirectResponse($redirect_path);
    }

    /**
     * @param Request $request
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function show(Request $request)
    {

        $this->twig_template .= '/Welcome/views/';

        $redirect = $request->getPathInfo();
        $this->render('login.html', ['redirect' => $redirect]);

    }
}